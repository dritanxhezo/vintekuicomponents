package com.vintek.ui.component;

import java.io.Serializable;

public class Field implements Serializable {
	private static final long serialVersionUID = -7937580685723111979L;
	
	private String label;
    private String value;

    public Field() {
        //
    }

    public Field(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}