package com.vintek.ui.component;

import java.util.List;

import javax.faces.component.FacesComponent;
import javax.faces.component.UINamingContainer;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

@FacesComponent(value="dynamicFieldList") // To be specified in componentType attribute.
@SuppressWarnings({"rawtypes", "unchecked"}) // We don't care about the actual model item type anyway.
public class DynamicFieldList extends UINamingContainer {

    private transient DataModel model;

    public void add() {
        getList().add(new Field("somelabel"));
    }

    public void remove() {
        getList().remove(model.getRowData());
    }

    public DataModel getModel() {
        if (model == null) model = new ListDataModel(getList());
        return model;
    }

    private List getList() { // Don't make this method public! Ends otherwise in an infinite loop calling itself everytime.
        return (List) getAttributes().get("list");
    }

}