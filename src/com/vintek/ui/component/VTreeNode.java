package com.vintek.ui.component;

import java.util.Arrays;
import java.util.List;

import org.jboss.logging.Logger;

public class VTreeNode {
	
	Logger log = Logger.getLogger(VTreeNode.class);
	
	public static final String ACCESS_FILE_TYPE = "access";
	public static final String BINARY_FILE_TYPE = "binary";
	public static final String BOOKMARK_FILE_TYPE = "bookmark";
	public static final String CODE_FILE_TYPE = "code";
	public static final String CVS_FILE_TYPE = "excel-csv";
	public static final String XLS_FILE_TYPE = "excel";
	public static final String MPEG_FILE_TYPE = "film";
	public static final String FLASH_FILE_TYPE = "flash-movie";
	public static final String HTML_FILE_TYPE = "globe";
	public static final String EPS_FILE_TYPE = "illustrator";
	public static final String JPG_FILE_TYPE = "image";
	public static final String MP3_FILE_TYPE = "music";
	public static final String OFFICE_FILE_TYPE = "office";
	public static final String PDF_FILE_TYPE = "pdf";
	public static final String PSD_FILE_TYPE = "photoshop";
	public static final String PPT_FILE_TYPE = "powerpoint";
	public static final String TXT_FILE_TYPE = "text";
	public static final String DOC_FILE_TYPE = "word";
	public static final String ZIP_FILE_TYPE = "zipper";

	public static final String BOOKMARK_FOLDER_TYPE = "bookmark";
	public static final String MP3_FOLDER_TYPE = "open-document-music";
	public static final String TXT_FOLDER_TYPE = "open-document-text";
	public static final String DOC_FOLDER_TYPE = "open-document";
	public static final String MPEG_FOLDER_TYPE = "open-film";
	public static final String JPG_FOLDER_TYPE = "open-image";
	public static final String TBL_FOLDER_TYPE = "open-table";
	public static final String ZIP_FOLDER_TYPE = "zipper";
	
	private String name;
	private String type;
	private String value;
    private List<VTreeNode> children;
	private VTreeNode selected;

 
    public VTreeNode(String name, VTreeNode... children) {
        this.name = name;
        this.children = Arrays.asList(children);
    }
 
    public VTreeNode(String name, String type, String value, VTreeNode... children) {
    	this(name, children);
        this.type = type;
        this.value = value;
    }
    
    public List<VTreeNode>getChildren() {
        return children;
    }

    public int getChildrenCount() {
        return children.size();
    }
     
    public boolean isHasChildren() {
        return children.size() > 0;
    }
 
    public String getName() {
        return name;
    }

	public String getType() {
		return type;
	}

	public String getValue() {
		return value;
	}

	public String setSelected(VTreeNode treeNode) {
		this.selected = treeNode;
		log.error("setSelected called with value " + treeNode.getValue());
		return null;
	}

	public VTreeNode getSelected() {
		return selected;
	}	
}
