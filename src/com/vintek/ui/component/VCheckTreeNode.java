package com.vintek.ui.component;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.jboss.logging.Logger;

public class VCheckTreeNode {
	
	Logger log = Logger.getLogger(VCheckTreeNode.class);
	
	private String name;
	private Boolean value;
    private List<VCheckTreeNode> children;

 
    public VCheckTreeNode(String name, VCheckTreeNode... children) {
        this.name = name;
        this.children = Arrays.asList(children);
    }
 
    public VCheckTreeNode(String name, Boolean value, VCheckTreeNode... children) {
    	this(name, children);
        this.value = value;
    }
    
    public List<VCheckTreeNode>getChildren() {
        return children;
    }

    public int getChildrenCount() {
        return children.size();
    }
     
    public boolean isHasChildren() {
        return children.size() > 0;
    }
 
    public String getName() {
        return name;
    }

	public Boolean getValue() {
		return value;
	}
	
	public void setValue(Boolean value) {
		this.value = value;
	}
		
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}	
}
