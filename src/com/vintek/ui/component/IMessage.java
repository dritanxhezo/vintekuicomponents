package com.vintek.ui.component;

public interface IMessage {
	public String getMessageType();
	public String getMessageString();
	public String getMessageDate();
}
