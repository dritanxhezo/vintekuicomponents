(function($) {

	$.fn.treeView = function() {
		var root = this;
        this.find('.branch').each(function() {
        	$(this).children('a, span.toggle').click(function() {
        		$(this).closest('li').children('ul').toggle('slow');
        		$(this).closest('li').toggleClass('closed');
       	 	});
        });

        this.find('a.file').each(function() {
        	$(this).click(function() {
        		root.find('.selected').toggleClass('selected');
        		$(this).toggleClass('selected');
       	 	});
        });
        
		return this;
	};
})(jQuery);