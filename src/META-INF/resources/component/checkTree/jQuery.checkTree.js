/**
 * jQuery Tree Control
 *
 * @author Maxim Vasiliev
 */
(function($){

	var CLASS_JQUERY_TREE = 'jquery-tree';
	var CLASS_JQUERY_TREE_CONTROLS = 'jquery-tree-controls';
	var CLASS_JQUERY_TREE_COLLAPSE_ALL = 'jquery-tree-collapseall';
	var CLASS_JQUERY_TREE_EXPAND_ALL = 'jquery-tree-expandall';
	var CLASS_JQUERY_TREE_COLLAPSED = 'jquery-tree-collapsed';
	var CLASS_JQUERY_TREE_HANDLE = 'jquery-tree-handle';
	var CLASS_JQUERY_TREE_TITLE = 'jquery-tree-title';
	var CLASS_JQUERY_TREE_NODE = 'jquery-tree-node';
	var CLASS_JQUERY_TREE_LEAF = 'jquery-tree-leaf';
	var CLASS_JQUERY_TREE_CHECKED = 'jquery-tree-checked';
	var CLASS_JQUERY_TREE_UNCHECKED = 'jquery-tree-unchecked';
	var CLASS_JQUERY_TREE_CHECKED_PARTIAL = 'jquery-tree-checked-partial';

	var COLLAPSE_ALL_CODE = '<span class="' + CLASS_JQUERY_TREE_COLLAPSE_ALL + '">Collapse All</span>';
	var EXPAND_ALL_CODE = '<span class="' + CLASS_JQUERY_TREE_EXPAND_ALL + '">Expand All</span>';
	var TREE_CONTROLS_CODE = '<div class="' + CLASS_JQUERY_TREE_CONTROLS + '">' + COLLAPSE_ALL_CODE + EXPAND_ALL_CODE + '</div>';

	var TREE_NODE_HANDLE_CODE = '<div class="' + CLASS_JQUERY_TREE_HANDLE + '">&nbsp;</div>';
	var TREE_NODE_HANDLE_COLLAPSED = "&nbsp;"; //&#160;
	var TREE_NODE_HANDLE_EXPANDED = "&nbsp;";

	$.fn.extend({

		/**
		 * The tree structure of the form:
		 * <ul>
		 *   <li><label><input type="checkbox" />Item1</label></li>
		 *   <li>
		 *     <label><input type="checkbox" />ItemWithSubitems</label>
		 *     <ul>
		 *       <li><label><input type="checkbox" />Subitem1</label></li>
		 *     </ul>
		 *   </li>
		 * </ul>
		 */
		checkTree: function(){
			// Add control for the entire tree (all the minimize, maximize, etc.), and add the class
			$(this)
				.addClass(CLASS_JQUERY_TREE)
				.before(TREE_CONTROLS_CODE)
				.prev('.' + CLASS_JQUERY_TREE_CONTROLS)
				.find('.' + CLASS_JQUERY_TREE_COLLAPSE_ALL).click(function(){
					$(this).parent().next('.' + CLASS_JQUERY_TREE)
						.find('li:has(ul)')
						.addClass(CLASS_JQUERY_TREE_COLLAPSED)
						.find('.' + CLASS_JQUERY_TREE_HANDLE)
						.html(TREE_NODE_HANDLE_COLLAPSED);
				})

				.parent('.' + CLASS_JQUERY_TREE_CONTROLS).find('.' + CLASS_JQUERY_TREE_EXPAND_ALL)
					.click(function(){
						$(this).parent().next('.' + CLASS_JQUERY_TREE)
							.find('li:has(ul)')
								.removeClass(CLASS_JQUERY_TREE_COLLAPSED)
							.find('.' + CLASS_JQUERY_TREE_HANDLE)
								.html(TREE_NODE_HANDLE_EXPANDED);
					});

			$('li', this).find(':first').addClass(CLASS_JQUERY_TREE_TITLE)
				.closest('li').addClass(CLASS_JQUERY_TREE_LEAF);

			// For all items, which are nodes (having children) ...
			$('li:has(ul:has(li))', this).find(':first')
				// ... add the element opening / closing unit
				.before(TREE_NODE_HANDLE_CODE)
				// ... add to the container class "tree node" and "turn back."
				.closest('li')
					.addClass(CLASS_JQUERY_TREE_NODE)
					//.addClass(CLASS_JQUERY_TREE_COLLAPSED)
					.removeClass(CLASS_JQUERY_TREE_LEAF);

			// ... bind click handler
			$('.' + CLASS_JQUERY_TREE_HANDLE, this).bind('click', function(){
				var leafContainer = $(this).parent('li');
				var leafHandle = leafContainer.find('>.' + CLASS_JQUERY_TREE_HANDLE);

				leafContainer.toggleClass(CLASS_JQUERY_TREE_COLLAPSED);

				if (leafContainer.hasClass(CLASS_JQUERY_TREE_COLLAPSED))
					leafHandle.html(TREE_NODE_HANDLE_COLLAPSED);
				else
					leafHandle.html(TREE_NODE_HANDLE_EXPANDED);
			});

			// Adding processing click on checkboxes
			$('input:checkbox', this).click(function(){
				setLabelClass(this);
				checkCheckbox(this);
			})
			// Putting checkboxes original classes
			.each(function(){
				setLabelClass(this);
				if (this.checked)
					checkParentCheckboxes(this);
			})
			// IE to wire up the label
			.closest('label').click(function(){
				labelClick(this);
				checkCheckbox($('input:checkbox', this));
			});
		}

	});

	/**
	 * Recursively checks whether all the checkboxes in the subtree of the parent node are selected.
	 * If no check box is selected - remove the check from the parent checkbox
	 * If at least one but not all - exposes class CLASS_JQUERY_TREE_CHECKED_PARTIAL parent checkbox
	 * If all - puts a check on the parent checkbox
	 *
	 * @param {Object} checkboxElement current checkbox
	*/	 
	function checkParentCheckboxes(checkboxElement){
		if (typeof checkboxElement == 'undefined' || !checkboxElement)
			return;

		// investigate whether checkboxes marked / partially allocated to the higher level
		var closestNode = $(checkboxElement).closest('ul');
		var allCheckboxes = closestNode.find('input:checkbox');
		var checkedCheckboxes = closestNode.find('input:checkbox:checked');

		var allChecked = allCheckboxes.length == checkedCheckboxes.length;

		var parentCheckbox = closestNode.closest('li').find('>.' + CLASS_JQUERY_TREE_TITLE + ' input:checkbox');

		if (parentCheckbox.length > 0) {
			parentCheckbox.get(0).checked = allChecked;

			if (!allChecked && checkedCheckboxes.length > 0)
				parentCheckbox.closest('label')
					.addClass(CLASS_JQUERY_TREE_CHECKED_PARTIAL)
					.removeClass(CLASS_JQUERY_TREE_CHECKED)
					.removeClass(CLASS_JQUERY_TREE_UNCHECKED);
			else
				if (allChecked)
					parentCheckbox.closest('label')
						.removeClass(CLASS_JQUERY_TREE_CHECKED_PARTIAL)
						.removeClass(CLASS_JQUERY_TREE_UNCHECKED)
						.addClass(CLASS_JQUERY_TREE_CHECKED);
				else
					parentCheckbox.closest('label')
						.removeClass(CLASS_JQUERY_TREE_CHECKED_PARTIAL)
						.removeClass(CLASS_JQUERY_TREE_CHECKED)
						.addClass(CLASS_JQUERY_TREE_UNCHECKED);

			checkParentCheckboxes(parentCheckbox.get(0));
		}
	}

	/**
	 * If this checkbox has child nodes - change their status
	 * on the current state of the checkbox
	 *
	 * @param {Object} checkboxElement current checkbox
	*/	 
	function checkCheckbox(checkboxElement){
		// check / uncheck underlying checkboxes
		$(checkboxElement).closest('li').find('input:checkbox').each(function(){
			this.checked = $(checkboxElement).attr('checked');
			setLabelClass(this);
		});
		checkParentCheckboxes(checkboxElement);
	};

	/**
	 * Exposes a class label depending on the checkbox
	 *
	 * @param {Object} checkboxElement checkbox
	 */
	function setLabelClass(checkboxElement){
		isChecked = $(checkboxElement).attr('checked');

		if (isChecked) {
			$(checkboxElement).closest('label')
				.addClass(CLASS_JQUERY_TREE_CHECKED)
				.removeClass(CLASS_JQUERY_TREE_UNCHECKED)
				.removeClass(CLASS_JQUERY_TREE_CHECKED_PARTIAL);
		}
		else {
			$(checkboxElement).closest('label')
				.addClass(CLASS_JQUERY_TREE_UNCHECKED)
				.removeClass(CLASS_JQUERY_TREE_CHECKED)
				.removeClass(CLASS_JQUERY_TREE_CHECKED_PARTIAL);
		}
	};

	/**
	 * Handles click on the label (for IE6)
	 */
	function labelClick(labelElement){
		var checkbox = $('input:checkbox', labelElement);
		var checked = checkbox.attr('checked');
		checkbox.attr('checked', !checked);
		setLabelClass(checkbox);
	}

})(jQuery);
