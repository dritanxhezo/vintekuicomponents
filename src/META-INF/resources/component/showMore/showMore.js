var showMore = function() {
	return {
		init:function(config){
			jQuery(document).ready(function($){
				config.$panel = $("#"+config.panelid);
				config.$less = $("#"+config.lessid);
				config.$more = $("#"+config.moreid);
				
				config.$less.click(function(event){
					config.$panel.animate({"height": config.lessHeight}, "fast");
					showMore.toggleMoreLess(config);
					event.preventDefault();
				});
				config.$more.click(function(event){
					if(config.moreHeight!='') {
						config.$panel.animate({"height": config.moreHeight}, "fast");
					} else {
						config.$panel.animateAuto("height", "fast");
					}
					showMore.toggleMoreLess(config);
					event.preventDefault();
				});			
			});
		},
		
		toggleMoreLess:function(config) {
			config.$less.toggle();
			config.$more.toggle();
		},
	};
}();

//
//Intended Fix for height/width animation of "auto" val 
//
jQuery.fn.animateAuto = function(prop, speed, callback){
 var elem, temp;
 return this.each(function(i, el){
     el = jQuery(el); 
     elem = el.clone().css(prop,"auto").insertBefore(el);
     temp = elem.css(prop);
     elem.remove();
     if(prop === "height")
         el.animate({"height":temp}, speed, callback);
     else if(prop === "width")
         el.animate({"width":temp}, speed, callback); 
 });  
};
